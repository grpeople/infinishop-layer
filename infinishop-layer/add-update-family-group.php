<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

// Load JSON encoded data sent from Infinisync
$parent_category = json_decode(file_get_contents("php://input"));

$family_group = $parent_category[0];
$name = $parent_category[1];

if ($id_parent_category = getParentCategoryIdByFamilyGroup($family_group)) {
    updateParentCategory($id_parent_category, $name);
} else {
    if (!addNewParentCategory($family_group, $name)) {
        http_send_status(500);
        exit();
    }
}

echo 'OK';
