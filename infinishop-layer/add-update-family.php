<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

// Load JSON encoded data sent from Infinisync
$category = json_decode(file_get_contents("php://input"));

$family = $category[0];
$family_group = $category[1];
$name = $category[2];

if ($id_category = getCategoryIdByFamily($family)) {
    updateCategory($id_category, $family_group, $name);
} else {
    if (!addNewCategory($family, $family_group, $name)) {
        http_send_status(500);
        exit();
    }
}

echo 'OK';
