<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

// Load JSON encoded data sent from Infinisync
$manufacturer = json_decode(file_get_contents("php://input"));

$precode = $manufacturer[0];
$name = $manufacturer[1];

if ($id_manufacturer = getManufacturerIdByPrecode($precode)) {
    updateManufacturer($id_manufacturer, $name);
} else {
    if (!addNewManufacturer($precode, $name)) {
        http_send_status(500);
        exit();
    }
}

echo 'OK';
