<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

// Load JSON encoded data sent from Infinisync
$product = json_decode(file_get_contents("php://input"));

$item = $product[0];
$precode = $product[1];
$family = $product[2];
$name = $product[3];
$price = $product[4];
$quantity = $product[5];
$width = $product[6];
$height = $product[7];
$depth = $product[8];
$weight = $product[9];
$active = $product[10];

if ($id_product = getProductIdByCode($item, $precode)) {
    updateProduct($id_product, $family, $name, $price, $quantity, $width, $height, $depth, $weight, $active);
} else {
    if (!addNewProduct($item, $precode, $family, $name, $price, $quantity, $width, $height, $depth, $weight, $active)) {
        http_send_status(500);
        exit();
    }
}

echo 'OK';
