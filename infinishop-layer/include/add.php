<?php

// Functions to add new elements to Prestashop

function addNewManufacturer($precode, $name)
{
    $manufacturer = new Manufacturer(null, _CONTEXT_LANG_ID_);
    $manufacturer->name = $name;
    $manufacturer->active = true;
    $manufacturer->add();
    return Db::getInstance()->insert('infinishop_manufacturers_reference',
        array('id_manufacturer' => $manufacturer->id, 'precode' => $precode));
}

function addNewParentCategory($family_group, $name)
{
    $parent_category = new Category(null, _CONTEXT_LANG_ID_);
    $parent_category->description = 'd';
    $parent_category->id_parent = 2;
    $parent_category->is_root_category = false;
    $parent_category->link_rewrite = 'lr';
    $parent_category->meta_description = 'md';
    $parent_category->meta_keywords = 'mk';
    $parent_category->meta_title = 'mt';
    $parent_category->name = $name;
    $parent_category->active = true;
    $parent_category->add();
    return Db::getInstance()->insert('infinishop_parent_categories_reference',
        array('id_parent' => $parent_category->id, 'family_group' => $family_group));
}

function addNewCategory($family, $family_group, $name)
{
    $category = new Category(null, _CONTEXT_LANG_ID_);
    $category->description = 'd';
    $category->id_parent = getParentCategoryIdByFamilyGroup($family_group);
    $category->is_root_category = false;
    $category->link_rewrite = 'lr';
    $category->meta_description = 'md';
    $category->meta_keywords = 'mk';
    $category->meta_title = 'mt';
    $category->name = $name;
    $category->active = true;
    $category->add();
    return Db::getInstance()->insert('infinishop_categories_reference',
        array('id_category' => $category->id, 'id_parent' => $category->id_parent, 'family' => $family));
}

function addNewProduct($code, $precode, $family, $name, $price, $quantity, $width, $height, $depth, $weight, $active)
{
    $product = new Product(null, false, _CONTEXT_LANG_ID_);
    $product->reference = $code;
    $product->id_manufacturer = getManufacturerIdByPrecode($precode);
    $product->id_category_default = getCategoryIdByFamily($family);
    $product->name = $name;
    $product->price = $price;
    $product->quantity = $quantity;
    $product->width = $width;
    $product->height = $height;
    $product->depth = $depth;
    $product->weight = $weight;
    $product->active = $active;
    $product->add();
    StockAvailable::setQuantity($product->id, 0, $quantity);
    return Db::getInstance()->insert('infinishop_products_reference',
        array('id_product' => $product->id, 'item' => $code, 'precode' => $precode));
}
