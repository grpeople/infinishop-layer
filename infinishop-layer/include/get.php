<?php

// Functions to get data from Prestashop

function getManufacturerIdByPrecode($precode)
{
    $sql = 'SELECT id_manufacturer
            FROM ' . _DB_PREFIX_ . 'infinishop_manufacturers_reference
            WHERE precode = \'' . $precode . '\' ; ';
    return Db::getInstance()->getValue($sql);
}

function getParentCategoryIdByFamilyGroup($family_group)
{
    $sql = 'SELECT id_parent
            FROM ' . _DB_PREFIX_ . 'infinishop_parent_categories_reference
            WHERE family_group = \'' . $family_group . '\' ; ';
    return Db::getInstance()->getValue($sql);
}

function getCategoryIdByFamily($family)
{
    $sql = 'SELECT id_category
            FROM ' . _DB_PREFIX_ . 'infinishop_categories_reference
            WHERE family = \'' . $family . '\' ; ';
    return Db::getInstance()->getValue($sql);
}

function getProductIdByCode($code, $precode)
{
    $sql = 'SELECT id_product
            FROM ' . _DB_PREFIX_ . 'infinishop_products_reference
            WHERE item = \'' . $code . '\'
            AND precode = \'' . $precode . '\' ; ';
    return Db::getInstance()->getValue($sql);
}
