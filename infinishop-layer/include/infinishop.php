<?php

// Infinishop requirements
// Module functions
require_once(dirname(__FILE__) . '/add.php');
require_once(dirname(__FILE__) . '/get.php');
require_once(dirname(__FILE__) . '/update.php');

// GLOBAL SHARED CONSTANTS USED BY THE MODULE ITSELF
define('_CONTEXT_LANG_ID_', Context::getContext()->language->id);
