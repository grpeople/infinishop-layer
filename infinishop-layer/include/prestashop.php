<?php

// Prestashop requirements
require_once(dirname(__FILE__).'/../../config/config.inc.php');
require_once(dirname(__FILE__).'/../../init.php');

if (!defined('_PS_VERSION_')) {
    http_send_status(500);
    exit('500 - Cannot load requirements');
}
