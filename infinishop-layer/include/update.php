<?php

// Functions to update elements already on Prestashop

function updateManufacturer($id_manufacturer, $name)
{
    $manufacturer = new Manufacturer($id_manufacturer, _CONTEXT_LANG_ID_);
    $manufacturer->name = $name;
    $manufacturer->active = true;
    $manufacturer->update();
}

function updateParentCategory($id_parent, $name)
{
    $parent_category = new Category($id_parent, _CONTEXT_LANG_ID_);
    $parent_category->name = $name;
    $parent_category->active = true;
    $parent_category->update();
}

function updateCategory($id_category, $family_group, $name)
{
    $category = new Category($id_category, _CONTEXT_LANG_ID_);
    $category->id_parent = getParentCategoryIdByFamilyGroup($family_group);
    $category->name = $name;
    $category->active = true;
    $category->update();
}

function updateProduct($id_product, $family, $name, $price, $quantity, $width, $height, $depth, $weight, $active)
{
    $product = new Product($id_product, false, _CONTEXT_LANG_ID_);
    $product->id_category_default = getCategoryIdByFamily($family);
    $product->name = $name;
    $product->price = $price;
    $product->quantity = $quantity;
    $product->width = $width;
    $product->height = $height;
    $product->depth = $depth;
    $product->weight = $weight;
    $product->active = $active;
    $product->update();
    StockAvailable::setQuantity($product->id, 0, $quantity);
}
