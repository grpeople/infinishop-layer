<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

// Creating all the reference tables
// Creating Manufacturer reference table
$sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'infinishop_manufacturers_reference (
            id_manufacturer int(10) UNSIGNED PRIMARY KEY NOT NULL UNIQUE,
            precode varchar(4) NOT NULL UNIQUE
        ) ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

// Creating Parent category reference table
$sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'infinishop_parent_categories_reference (
            id_parent int(10) UNSIGNED PRIMARY KEY NOT NULL UNIQUE,
            family_group varchar(20) NOT NULL UNIQUE
        ) ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

// Creating Category reference table
$sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'infinishop_categories_reference (
            id_category int(10) UNSIGNED PRIMARY KEY NOT NULL UNIQUE,
            id_parent int(10) UNSIGNED NOT NULL,
            family varchar(10) NOT NULL UNIQUE
        ) ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

// Creating Product reference table
$sql = 'CREATE TABLE IF NOT EXISTS ' . _DB_PREFIX_ . 'infinishop_products_reference (
            id_product int(10) UNSIGNED PRIMARY KEY NOT NULL UNIQUE,
            item varchar(21) NOT NULL,
            precode varchar(4) NOT NULL
        ) ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

echo 'OK';
