<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

if (!Db::getInstance()->execute('SELECT "OK" ; ')) {
    http_send_status(500);
    exit();
}

echo 'OK';