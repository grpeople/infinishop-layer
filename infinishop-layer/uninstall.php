<?php

require_once(dirname(__FILE__) . '/include/prestashop.php');
require_once(dirname(__FILE__) . '/include/infinishop.php');

// Dropping all the reference tables
// Dropping Manufacturer reference table
$sql = 'DROP TABLE ' . _DB_PREFIX_ . 'infinishop_manufacturers_reference ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

// Dropping Parent category reference table
$sql = 'DROP TABLE ' . _DB_PREFIX_ . 'infinishop_parent_categories_reference ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

// Dropping Category reference table
$sql = 'DROP TABLE ' . _DB_PREFIX_ . 'infinishop_categories_reference ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

// Dropping Product reference table
$sql = 'DROP TABLE ' . _DB_PREFIX_ . 'infinishop_products_reference ; ';
if (!Db::getInstance()->execute($sql)) {
    http_send_status(500);
    exit();
}

echo 'OK';